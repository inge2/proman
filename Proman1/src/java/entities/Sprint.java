/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Andrea
 */
@Entity
@Table(name = "sprint")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Sprint.findAll", query = "SELECT s FROM Sprint s")
    , @NamedQuery(name = "Sprint.findBySprintId", query = "SELECT s FROM Sprint s WHERE s.sprintId = :sprintId")
    , @NamedQuery(name = "Sprint.findBySprintFecini", query = "SELECT s FROM Sprint s WHERE s.sprintFecini = :sprintFecini")
    , @NamedQuery(name = "Sprint.findBySprintFecfin", query = "SELECT s FROM Sprint s WHERE s.sprintFecfin = :sprintFecfin")
    , @NamedQuery(name = "Sprint.findBySprintDesc", query = "SELECT s FROM Sprint s WHERE s.sprintDesc = :sprintDesc")})
public class Sprint implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "sprint_id")
    private Short sprintId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "sprint_fecini")
    private short sprintFecini;
    @Basic(optional = false)
    @NotNull
    @Column(name = "sprint_fecfin")
    @Temporal(TemporalType.DATE)
    private Date sprintFecfin;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "sprint_desc")
    private String sprintDesc;
    @JoinColumn(name = "proyecto_id", referencedColumnName = "proyecto_id")
    @ManyToOne(optional = false)
    private Proyecto proyectoId;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "sprintId")
    private Collection<Userstory> userstoryCollection;

    public Sprint() {
    }

    public Sprint(Short sprintId) {
        this.sprintId = sprintId;
    }

    public Sprint(Short sprintId, short sprintFecini, Date sprintFecfin, String sprintDesc) {
        this.sprintId = sprintId;
        this.sprintFecini = sprintFecini;
        this.sprintFecfin = sprintFecfin;
        this.sprintDesc = sprintDesc;
    }

    public Short getSprintId() {
        return sprintId;
    }

    public void setSprintId(Short sprintId) {
        this.sprintId = sprintId;
    }

    public short getSprintFecini() {
        return sprintFecini;
    }

    public void setSprintFecini(short sprintFecini) {
        this.sprintFecini = sprintFecini;
    }

    public Date getSprintFecfin() {
        return sprintFecfin;
    }

    public void setSprintFecfin(Date sprintFecfin) {
        this.sprintFecfin = sprintFecfin;
    }

    public String getSprintDesc() {
        return sprintDesc;
    }

    public void setSprintDesc(String sprintDesc) {
        this.sprintDesc = sprintDesc;
    }

    public Proyecto getProyectoId() {
        return proyectoId;
    }

    public void setProyectoId(Proyecto proyectoId) {
        this.proyectoId = proyectoId;
    }

    @XmlTransient
    public Collection<Userstory> getUserstoryCollection() {
        return userstoryCollection;
    }

    public void setUserstoryCollection(Collection<Userstory> userstoryCollection) {
        this.userstoryCollection = userstoryCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (sprintId != null ? sprintId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Sprint)) {
            return false;
        }
        Sprint other = (Sprint) object;
        if ((this.sprintId == null && other.sprintId != null) || (this.sprintId != null && !this.sprintId.equals(other.sprintId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Sprint[ sprintId=" + sprintId + " ]";
    }
    
}
