/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Andrea
 */
@Entity
@Table(name = "proyecto")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Proyecto.findAll", query = "SELECT p FROM Proyecto p")
    , @NamedQuery(name = "Proyecto.findByProyectoId", query = "SELECT p FROM Proyecto p WHERE p.proyectoId = :proyectoId")
    , @NamedQuery(name = "Proyecto.findByProyectoNombre", query = "SELECT p FROM Proyecto p WHERE p.proyectoNombre = :proyectoNombre")
    , @NamedQuery(name = "Proyecto.findByProyectoFecini", query = "SELECT p FROM Proyecto p WHERE p.proyectoFecini = :proyectoFecini")
    , @NamedQuery(name = "Proyecto.findByProyectoFecfin", query = "SELECT p FROM Proyecto p WHERE p.proyectoFecfin = :proyectoFecfin")})
public class Proyecto implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "proyecto_id")
    private Short proyectoId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "proyecto_nombre")
    private String proyectoNombre;
    @Basic(optional = false)
    @NotNull
    @Column(name = "proyecto_fecini")
    @Temporal(TemporalType.DATE)
    private Date proyectoFecini;
    @Basic(optional = false)
    @NotNull
    @Column(name = "proyecto_fecfin")
    @Temporal(TemporalType.DATE)
    private Date proyectoFecfin;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "proyectoId")
    private Collection<Backlog> backlogCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "proyectoId")
    private Collection<Sprint> sprintCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "proyectoId")
    private Collection<Rolusuario> rolusuarioCollection;

    public Proyecto() {
    }

    public Proyecto(Short proyectoId) {
        this.proyectoId = proyectoId;
    }

    public Proyecto(Short proyectoId, String proyectoNombre, Date proyectoFecini, Date proyectoFecfin) {
        this.proyectoId = proyectoId;
        this.proyectoNombre = proyectoNombre;
        this.proyectoFecini = proyectoFecini;
        this.proyectoFecfin = proyectoFecfin;
    }

    public Short getProyectoId() {
        return proyectoId;
    }

    public void setProyectoId(Short proyectoId) {
        this.proyectoId = proyectoId;
    }

    public String getProyectoNombre() {
        return proyectoNombre;
    }

    public void setProyectoNombre(String proyectoNombre) {
        this.proyectoNombre = proyectoNombre;
    }

    public Date getProyectoFecini() {
        return proyectoFecini;
    }

    public void setProyectoFecini(Date proyectoFecini) {
        this.proyectoFecini = proyectoFecini;
    }

    public Date getProyectoFecfin() {
        return proyectoFecfin;
    }

    public void setProyectoFecfin(Date proyectoFecfin) {
        this.proyectoFecfin = proyectoFecfin;
    }

    @XmlTransient
    public Collection<Backlog> getBacklogCollection() {
        return backlogCollection;
    }

    public void setBacklogCollection(Collection<Backlog> backlogCollection) {
        this.backlogCollection = backlogCollection;
    }

    @XmlTransient
    public Collection<Sprint> getSprintCollection() {
        return sprintCollection;
    }

    public void setSprintCollection(Collection<Sprint> sprintCollection) {
        this.sprintCollection = sprintCollection;
    }

    @XmlTransient
    public Collection<Rolusuario> getRolusuarioCollection() {
        return rolusuarioCollection;
    }

    public void setRolusuarioCollection(Collection<Rolusuario> rolusuarioCollection) {
        this.rolusuarioCollection = rolusuarioCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (proyectoId != null ? proyectoId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Proyecto)) {
            return false;
        }
        Proyecto other = (Proyecto) object;
        if ((this.proyectoId == null && other.proyectoId != null) || (this.proyectoId != null && !this.proyectoId.equals(other.proyectoId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Proyecto[ proyectoId=" + proyectoId + " ]";
    }
    
}
